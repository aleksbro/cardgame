package edu.ntnu.idatt2001.CardGame;

import edu.ntnu.idatt2001.CardGame.CardBuilder.Card;
import edu.ntnu.idatt2001.CardGame.CardBuilder.Face;
import edu.ntnu.idatt2001.CardGame.CardBuilder.Suits;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class DeckOfCards {
    private final List<Card> cards = new ArrayList<>();

    public DeckOfCards() {
        for (Suits s : Suits.values()) {
            for (Face f : Face.values()) {
                Card c = new Card(s,f);
                cards.add(c);
            }
        }
    }

    /**
     * Method to save all cards.
     * @throws IOException
     */
    public void save() throws IOException {
        PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("cards.txt")));

        cards.forEach(out::println);
    }

    /**
     * Method for picking n cards.
     *
     * @param n
     * @return ArrayList
     */
    public HandOfCards pickCards(int n) throws IllegalArgumentException {
        if (n <= 0 | n > 52) {
            throw new  IllegalArgumentException("Number must be over 0 and below 52");
        }
        Random rand = new Random();
        ArrayList<Card> cardsClone = new ArrayList<>(cards);
        ArrayList<Card> hand = new ArrayList<>();


        for(int i = 0; i < n; i++) {
            int randomIndex = rand.nextInt(cardsClone.size());
            hand.add(cardsClone.get(randomIndex));
            cardsClone.remove(randomIndex);
        }

        return new HandOfCards(hand);
    }
}
