package edu.ntnu.idatt2001.CardGame;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.File;
import java.net.URL;

public class Main extends Application {


    @Override
    public void start(Stage primaryStage) throws Exception {
        try {

            //Parent root = FXMLLoader.load(getClass().getResource("/fxml/sample.fxml"));
            URL url = new File("C:\\Users\\Aleksander\\Skrivebord\\CardGame\\src\\main\\java\\edu\\ntnu\\idatt2001\\CardGame\\sample.fxml").toURI().toURL();
            Parent root = FXMLLoader.load(url);
            primaryStage.setTitle("Hello world");
            primaryStage.setScene(new Scene(root));
            primaryStage.show();

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public static void main(String [] args) {
        launch(args);
    }
}
