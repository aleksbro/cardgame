package edu.ntnu.idatt2001.CardGame;
import edu.ntnu.idatt2001.CardGame.CardBuilder.Card;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;


import javafx.event.ActionEvent;

import java.util.ArrayList;

public class Controller {

    @FXML
            private Label sumOfFaces;
    @FXML
            private Label flushLabel;
    @FXML
            private Label heartCards;
    @FXML
            private Label queenOfSpades;
    @FXML
            private Button dealingHand;
    @FXML
            private Button handCheck;
    @FXML
            private TextField handOfCards;

    DeckOfCards deck = new DeckOfCards();
    HandOfCards hand;


    public void dealHand(ActionEvent e) {
        hand = deck.pickCards(5);
        handOfCards.setText(hand.toString());
    }

    public void handCheck(ActionEvent e) {
        sumOfFaces.setText(String.valueOf(hand.getSum()));

        heartCards.setText(hand.getHearts());

        if(hand.findQueenOfSpades()) {
            queenOfSpades.setText("Yes");
        } else {
            queenOfSpades.setText("No");
        }

        if(hand.isFlush()) {
            flushLabel.setText("YES");
        } else {
            flushLabel.setText("No");
        }

    }
}
