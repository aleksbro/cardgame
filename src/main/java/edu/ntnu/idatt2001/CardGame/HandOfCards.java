package edu.ntnu.idatt2001.CardGame;

import edu.ntnu.idatt2001.CardGame.CardBuilder.Card;

import java.util.ArrayList;

public class HandOfCards {
    private final ArrayList<Card> cardsOnHand;
    public HandOfCards(ArrayList<Card> cardsOnHand) {
        if(cardsOnHand.isEmpty()) {
            throw new IllegalArgumentException();
        }
        this.cardsOnHand = cardsOnHand;
    }

    public int getSum(){
        int sum;
        sum = cardsOnHand.stream().mapToInt(Card::getFace).sum();
        return sum;
    }

    public String getHearts() {
        ArrayList<String> hCards = new ArrayList<>();
        cardsOnHand.stream().filter(h->h.getAssString().startsWith("H"))
                .forEach(heart -> hCards.add(heart.getAssString()));
        StringBuilder hearts = new StringBuilder();
        for (String h : hCards) {
            hearts.append(h).append("--");
        }
        if(hCards.size() == 0) {
            hearts.append("None");
        }
        return hearts.toString();
    }

    public Boolean findQueenOfSpades() {
        return cardsOnHand.stream().anyMatch(q -> q.getAssString().equals("QUEEN of SPADES"));
    }

    public Boolean isFlush() {
        int hearts = (int) cardsOnHand.stream().filter(h -> h.getSuit().equals("HEARTS")).count();
        int clubs = (int) cardsOnHand.stream().filter(c -> c.getSuit().equals("CLUBS")).count();
        int spades = (int) cardsOnHand.stream().filter(s -> s.getSuit().equals("SPADES")).count();
        int diamonds = (int) cardsOnHand.stream().filter(d -> d.getSuit().equals("DIAMONDS")).count();

        if(hearts == 5) {
            return true;
        } else if(clubs == 5) {
            return true;
        } else if(spades == 5) {
            return true;
        }else if (diamonds == 5) {
            return true;
        }

        return false;
    }

    public ArrayList<Card> getCardsOnHand() {
        return cardsOnHand;
    }

    public String toString() {
        StringBuilder show = new StringBuilder();
        show.append("--");
        for(Card c : cardsOnHand) {
            show.append(c.getAssString()).append("--");
        }
        return show.toString();
    }
}
