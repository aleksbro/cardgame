package edu.ntnu.idatt2001.CardGame.CardBuilder;

public class Card {
    private final Suits suit;
    private final Face face;

    public Card(Suits suit, Face face) {
        this.suit = suit;
        this.face = face;
    }

    public  String getAssString() {
        return String.format("%s of %s", face, suit);
    }

    public int getFace() {
        return face.getFace();
    }


    public String getSuit() {
        return suit.getSuit();
    }

}
