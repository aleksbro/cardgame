
module edu.ntnu.idatt.CardGame {
    requires javafx.controls;
    requires javafx.fxml;
    exports edu.ntnu.idatt2001.CardGame;
    opens edu.ntnu.idatt2001.CardGame to javafx.fxml;
}
